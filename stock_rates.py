"""
    @author: Douglas Johann

    Este programa obtem as cotacoes diarias de uma ativo da bolsa de valores B3
"""

import MetaTrader5  as mt5
#from pytz import timezone
from datetime import datetime, timedelta
import pandas as pd
import matplotlib.pyplot as plt
import pytz
import numpy as np
import argparse

def init_mt5():
    """
    conecta com metatreder 5
    """
    if not mt5.initialize():
        print("initialize() failed")
        mt5.shutdown()
 
# request connection status and parameters
#print(mt5.terminal_info())
# get data on MetaTrader 5 version
#print(mt5.version())

def get_stock_rates(data):
    """
    obtem rates da stock informada como argumento
    """
    timezone = pytz.timezone("America/Sao_Paulo")
    utc_from = datetime(int(data[2]), int(data[1]), int(data[0])+1, tzinfo=timezone)

    stock_rates = mt5.copy_rates_from(args.ativo.upper(), mt5.TIMEFRAME_M1, utc_from, 500)    

    return stock_rates

# shut down connection to MetaTrader 5
def shutdown_mt5():
    """
    finaliza conexao com metatrader 5
    """
    mt5.shutdown()

def plot_graphic(data, stock_rates):
    """
    plota grafico com as cotacoes do ativo informado
    """
    timezone = pytz.timezone("America/Sao_Paulo")
    utc_from = datetime(int(data[2]), int(data[1]), int(data[0]), tzinfo=timezone)

    keep = np.ones(stock_rates.shape, dtype = bool)
    for idx in range(len(stock_rates)):

        rates = stock_rates[idx]
        data_c = datetime.fromtimestamp(rates['time'])
        data_c = data_c.replace(tzinfo = timezone)

        #print(data_c)
        #print(utc_from)

        if(utc_from > data_c):
            keep[idx] = False

    stock_rates = stock_rates[keep]

    rates_frame = pd.DataFrame(stock_rates)
    rates_frame['time']=pd.to_datetime(rates_frame['time'], unit='s')

    #print(rates_frame)

    plt.plot(rates_frame['time'], rates_frame['close'], 'r-', label=args.ativo.upper())

    # display the legends
    plt.legend(loc='upper left')
     
    # add the header
    plt.title(args.ativo.upper())
     
    # display the chart
    plt.show()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="obtem as cotacoes diarias de um ativo da bolsa de valores B3", prefix_chars='-')
    parser.add_argument('-a', '--ativo', default='ITUB4', help='nome do ativo')
    parser.add_argument('-d', '--data', default='03/07/2020', help='data')

    args = parser.parse_args()

    init_mt5()

    data = args.data.split('/')
    stock_rates = get_stock_rates(data)

    shutdown_mt5()

    plot_graphic(data, stock_rates)
